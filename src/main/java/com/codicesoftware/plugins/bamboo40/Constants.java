package com.codicesoftware.plugins.bamboo40;

public class Constants {
	public static final String REPO_PREFIX = "custom.repository.plasticscm.";

	// These match the tags in the resource templates
	public static final String PLASTIC_CM_EXE = REPO_PREFIX + "cmExe";

	public static final String PLASTIC_REPOSITORY = REPO_PREFIX + "repositoryname";
    public static final String PLASTIC_REPOSITORYSERVER = REPO_PREFIX + "repositoryserver";
    public static final String PLASTIC_BRANCH_TO_TRACK = REPO_PREFIX + "branch";

	// This matches the field name in the admin config screen
	public static final String PLASTIC_CM_EXE_FIELD = "cmExePath";
	// Default configuration
	public static final String PLASTIC_DEFAULT_CM_EXE = "cm";
	public static final String PLASTIC_DEFAULT_SELECTOR = "repository \"default\"\n  path \"/\"\n    smartbranch \"/main\"\n";
    public static final String PLASTIC_WORKSPACE_SELECTOR_FORMAT = "repository \"%s\"\n  path \"/\"\n    smartbranch \"%s\" changeset \"%s\"\n";

	public static final String NAME = "Plastic SCM";

    public static final String BR_NAME = "BRANCH_NAME";
	public static final String BRANCH_PREFIX ="br:";
    public static final String MAIN = "/main";
    public static final String PLASTIC_DEFAULT_REPOSITORY_NAME = "default";
}
