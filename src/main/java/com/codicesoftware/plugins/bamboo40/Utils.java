package com.codicesoftware.plugins.bamboo40;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.Logger;
import com.codicesoftware.plastic.commands.GetConfigCommand;
import com.codicesoftware.plastic.commontypes.BranchInfo;
import com.codicesoftware.plastic.query.QueryCommands;
import com.codicesoftware.plastic.core.PlasticException;
import com.atlassian.bamboo.repository.RepositoryException;

public class Utils
{
	private static final Logger log = Logger.getLogger(Utils.class);

	static String replaceBranchInSelector(String selector, String firstResolvedBranch, String pattern)
	{
		return selector.replace(pattern, firstResolvedBranch);
	}

	public static String getOnlyBranchName(String fullBranchName)
	{
		String[] branchNameAndParents = fullBranchName.split("/");
		return branchNameAndParents[branchNameAndParents.length - 1];
	}

	public static void writeToFile(String text, File file)
	{
		try
		{
			FileWriter fstream = new FileWriter(file);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(text);
			out.close();
		}
		catch (Exception ex)
		{
		    String message = "Could not write current branch to file: " + ex.getMessage();
			log.warn(message, ex);
		}
	}

	public static void writeToFile(String text, String fileName)
	{
		try
		{
			FileWriter fstream = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(text);
			out.close();
		}
		catch (Exception ex)
		{
		    String message = "Could not write current branch to file: " + ex.getMessage();
			log.warn(message, ex);
		}
	}

    public static String getDefaultRepServer() {
        String theValue = "";
        try
        {
            GetConfigCommand cmd = new GetConfigCommand("defaultrepserver");
            cmd.execute();
            theValue = cmd.getParameterValue();
        } catch (Exception e) {
            log.error("The getconfig command returned an error:" + e.getMessage());
            log.debug("The getconfig command returned an error:" + e.getMessage() + " - " + e.getStackTrace());
        }
        return theValue;

    }

    public static String getRepositorySpec(PlasticRepositoryData repositoryData) {
        return getRepositorySpec(repositoryData.repository,  repositoryData.repositoryserver);
    }

    private static String getRepositorySpec(String repName, String repServer) {

        if (repServer == null || repServer.length() == 0)  {
            return repName;
        }
        return repName + "@" + repServer;
    }


    public static BranchInfo getBranchInfo(String branch, String repName, String repServer) throws RepositoryException {
        String branchNameForFindCommand = getOnlyBranchName(branch);
        String repSpec = getRepositorySpec(repName, repServer);
        try
        {
            BranchInfo[] branchesByName = QueryCommands.GetBranchesByName(branchNameForFindCommand, repSpec);
            for (BranchInfo aBranchesByName : branchesByName)
                if (aBranchesByName.getFullBranchNameWithoutBranchPreffix().equals(branch))
                    return aBranchesByName;

            throw new RepositoryException(String.format(
                    "Unable to obtain a valid branch. Branch name:%s rep spec: %s",
                    branch, repSpec));

        }
        catch (PlasticException e)
        {
            String exceptionMessage = "Failed to obtain the specified tracked branch:" +e.getMessage();
            log.error(exceptionMessage);
            log.debug(exceptionMessage +" - " +e.getStackTrace());
            throw new RepositoryException(exceptionMessage);
        }
    }
}
