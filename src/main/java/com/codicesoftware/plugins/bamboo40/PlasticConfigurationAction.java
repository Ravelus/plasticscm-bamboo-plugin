package com.codicesoftware.plugins.bamboo40;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.codicesoftware.plastic.core.PlasticException;
import com.codicesoftware.plastic.core.PlasticShell;

public class PlasticConfigurationAction extends BambooActionSupport {
	private static final long serialVersionUID = -2079002128690913834L;

	private static final Logger log = Logger.getLogger(PlasticConfigurationAction.class);

	private String cmExePath;

	public void validate() {
		super.validate();

		if (StringUtils.isBlank(cmExePath)) {
			addFieldError(Constants.PLASTIC_CM_EXE_FIELD, "Please specifiy the cm path");
		}
		if (cmExePath != null && cmExePath.trim().length() != 0) {
			File exe = new File(cmExePath);
			if (exe.isAbsolute() && !exe.canExecute()) {
				addFieldError(Constants.PLASTIC_CM_EXE_FIELD, "The cm file specified is not executable");
			}
		}

		log.warn("PlasticRepository checking connection");

		try {
			PlasticShell.testConnection(cmExePath);
		} catch (PlasticException ex) {
			addFieldError(Constants.PLASTIC_CM_EXE_FIELD, "Connection test failed: " + ex.getMessage());
		}
	}

	public String doDefault() {
		AdministrationConfiguration administrationConfiguration = getAdministrationConfiguration();
		if(administrationConfiguration == null) {
			return "error";
		}

		String path = administrationConfiguration.getSystemProperty(Constants.PLASTIC_CM_EXE);
		if(path == null) {
			path = Constants.PLASTIC_DEFAULT_CM_EXE;
		}
		setCmExePath(path);

		return "input";
	}

	public String doExecute() {
		AdministrationConfiguration administrationConfiguration = getAdministrationConfiguration();
		if(administrationConfiguration == null) {
			return "error";
		}

		administrationConfiguration.setSystemProperty(Constants.PLASTIC_CM_EXE, cmExePath);
		this.administrationConfigurationManager.saveAdministrationConfiguration(administrationConfiguration);

		addActionMessage(getText("config.updated"));
		return "success";
	}

	public void setCmExePath(String cmExePath) {
		this.cmExePath = cmExePath;
	}

	public String getCmExePath() {
		return cmExePath;
	}
}

