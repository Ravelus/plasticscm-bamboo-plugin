package com.codicesoftware.plugins.bamboo40;

import java.io.Serializable;


public class PlasticRepositoryData implements Serializable
{
        public String repository = "";
        public String repositoryserver = "";
        public String branch = "";

        public PlasticRepositoryData cloneData() {
            PlasticRepositoryData data = new PlasticRepositoryData();
            data.repository = this.repository;
            data.repositoryserver = this.repositoryserver;
            data.branch = this.branch;
            return data;
        }

    public String printValues() {
        return String.format("server:%s repository: %s branch:%s", repository, repositoryserver, branch);
    }
}