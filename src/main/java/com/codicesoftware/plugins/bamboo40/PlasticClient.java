package com.codicesoftware.plugins.bamboo40;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bamboo.spring.ComponentAccessor;
import com.codicesoftware.plastic.core.EnviromentManager;
import com.codicesoftware.plastic.core.IProviderInfo;
import com.codicesoftware.plastic.core.PlasticException;
import com.google.common.base.Supplier;

public class PlasticClient implements IProviderInfo {
    public static final Supplier<AdministrationConfigurationManager> ADMINISTRATION_CONFIGURATION_MANAGER = ComponentAccessor.newLazyComponentReference("administrationConfigurationManager");
	public PlasticClient() {
		EnviromentManager.getInstance().initialize(this);
	}

	@Override
    public String getCmExePath() throws PlasticException {
        final AdministrationConfiguration administrationConfiguration = ADMINISTRATION_CONFIGURATION_MANAGER.get().getAdministrationConfiguration();
        String cmExePath = administrationConfiguration.getSystemProperty(Constants.PLASTIC_CM_EXE);
		if (cmExePath == null) {
			cmExePath = Constants.PLASTIC_DEFAULT_CM_EXE;
		}

		return cmExePath;
	}

}
