package com.codicesoftware.plugins.bamboo40.changesproviders;

import org.jetbrains.annotations.NotNull;
import org.apache.log4j.Logger;
import com.codicesoftware.plastic.commontypes.WorkspaceInfo;
import com.codicesoftware.plastic.commands.GetWorkspaceFromPathCommand;
import com.codicesoftware.plastic.commands.CreateWorkspaceCommand;
import com.codicesoftware.plastic.commands.SetWorkspaceSelectorCommand;
import com.codicesoftware.plastic.core.PlasticException;
import com.codicesoftware.plugins.bamboo40.Constants;
import com.codicesoftware.plugins.bamboo40.PlasticRepositoryData;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.util.concurrent.ManagedLock;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.ManagedLocks;

import java.io.File;


public class WorkspaceManager
{
    static final String PLASTIC_CACHE_DIRECTORY = "plastic-bamboo-workspaces";
    static final Function<File, ManagedLock> cacheLockFactory = ManagedLocks.weakManagedLockFactory();
    private static final Logger log = Logger.getLogger(WorkspaceManager.class);

    public static File getWorkspaceFilePath(File baseDir, PlasticRepositoryData repositoryData) {
        File cacheDirectory = new File(baseDir, PLASTIC_CACHE_DIRECTORY);
        String repositoryDataFolderName = getRepositoryDataFolderName(repositoryData);
        return new File(cacheDirectory, repositoryDataFolderName);
    }

    private static String getRepositoryDataFolderName(PlasticRepositoryData repositoryData) {
        return "bamboo40_" + repositoryData.repositoryserver.hashCode() + "_" +
                repositoryData.repository + "_" +
                repositoryData.branch.hashCode();
    }

    public static ManagedLock getCacheLock(@NotNull File cache) {
        return cacheLockFactory.get(cache);
    }

    public static boolean workspaceAlreadyExists(File workingDir) {
        return getWorkpsaceInfoFromPath(workingDir.getAbsolutePath()) != null;
    }

    private static WorkspaceInfo getWorkpsaceInfoFromPath(String wkPath) {
        try
        {
			GetWorkspaceFromPathCommand gwp = new GetWorkspaceFromPathCommand(wkPath);
			return gwp.execute();
		}
		catch (PlasticException ex) {
			String message = "Error getting workspace from path: " + ex.getMessage();
			log.warn(message, ex);
			return null;
		}
	}

    public static boolean createWorkspace(@NotNull File workingDir) throws RepositoryException {

        if (workspaceAlreadyExists(workingDir))
            return false;

        return doCreateWorkspace(workingDir);
	}

    private static boolean doCreateWorkspace(@NotNull File workingDir) throws RepositoryException {

        String wkName = workingDir.getName();
        String wkPath = workingDir.getAbsolutePath();

		try
        {
			CreateWorkspaceCommand mkwk = new CreateWorkspaceCommand(wkName, wkPath);
			mkwk.Execute();

            WorkspaceInfo wkInfo = getWorkpsaceInfoFromPath(wkPath);

			if(wkInfo == null) {
				String message = "Unable to create workspace name " + wkName + " at " + wkPath;
				log.warn(message);
				throw new RepositoryException(message);
			}
			return true;
		}
		catch (PlasticException ex) {
			String message = "Error creating workspace name " + wkName + " at " + wkPath + ": " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}
    }

    public static void updateWorkspace(
            @NotNull File workingDir,
            String repSpec,
            String branch,
            String vcsRevisionKey) throws RepositoryException
    {
        WorkspaceInfo wkInfo = getWorkpsaceInfoFromPath(workingDir.getAbsolutePath());
        setWorkspaceSelector(wkInfo, repSpec, branch, vcsRevisionKey);
    }

    private static void setWorkspaceSelector(WorkspaceInfo wkInfo, String repSpec, String branch, String vcsRevisionKey)
        throws RepositoryException {

        String selector = String.format(Constants.PLASTIC_WORKSPACE_SELECTOR_FORMAT,
                repSpec,
                branch,
                vcsRevisionKey);

		log.debug("setting selector: " + selector + "On path:" + wkInfo.getClientPath());

		try	{
			SetWorkspaceSelectorCommand cmd = new SetWorkspaceSelectorCommand(wkInfo, selector);
			cmd.execute();
		}
		catch (PlasticException ex) {
			String message = "Error setting selector: " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}
	}
}