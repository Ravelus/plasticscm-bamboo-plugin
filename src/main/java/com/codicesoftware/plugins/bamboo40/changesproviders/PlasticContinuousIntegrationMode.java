package com.codicesoftware.plugins.bamboo40.changesproviders;

import com.codicesoftware.plugins.bamboo40.PlasticRepositoryData;
import com.codicesoftware.plugins.bamboo40.Utils;
import com.codicesoftware.plastic.core.PlasticException;
import com.codicesoftware.plastic.query.WhereClauseBuilder;
import com.codicesoftware.plastic.query.QueryCommands;
import com.codicesoftware.plastic.commontypes.BranchInfo;
import com.codicesoftware.plastic.commontypes.ChangesetInfo;
import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.repository.RepositoryException;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.io.File;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;


public class PlasticContinuousIntegrationMode extends PlasticBaseProvider {

    private static final Logger log = Logger.getLogger(PlasticContinuousIntegrationMode.class);
    public PlasticContinuousIntegrationMode(PlasticRepositoryData repositoryData)  {
        super(repositoryData);
    }

    @Override
    public String getChangesSinceLastBuilt(String lastVcsKeyBuilt, List<Commit> newerCommits)
        throws RepositoryException	{
        if (newerCommits == null)
            newerCommits = new ArrayList<Commit>();

        return addCommitsInContinuousIntegrationMode(lastVcsKeyBuilt, newerCommits);
    }

    private String addCommitsInContinuousIntegrationMode(String lastVcsKeyBuilt, List<Commit> commits)
            throws RepositoryException {

        BranchInfo branchInfo = Utils.getBranchInfo(
                mRepositoryData.branch, mRepositoryData.repository, mRepositoryData.repositoryserver);
        String lastCsetOnRep = branchInfo.getChangeset();

        ChangesetInfo[] changesets = getChangesetsToBeCommited(branchInfo, lastVcsKeyBuilt);
        if (changesets.length == 0)
            return lastCsetOnRep;

        addChangesetsToPendingCommmits(changesets, commits);
        return lastCsetOnRep;
    }

    private ChangesetInfo[] getChangesetsToBeCommited(@NotNull BranchInfo brInfo, String lastCsetBuilt)
            throws RepositoryException	{

        long lastCsetBuiltNumber = -1;
        try {
            lastCsetBuiltNumber = Long.parseLong(lastCsetBuilt);
        }
        catch (Exception ex) {
            String message = "Error parsing the last changeset build (from vcsRevisionKey) repositories: " + ex.getMessage();
			log.error(message, ex);
        }

		WhereClauseBuilder revClause = new WhereClauseBuilder();
		revClause.addClause("changesetid", ">", lastCsetBuiltNumber);
		revClause.addClause("changesetid", "<=", Long.parseLong(brInfo.getChangeset()));
		revClause.addClause("branch", "=", brInfo.getName());

		try {
			//update the last changeset retrieved
			return QueryCommands.GetChangesets(null, revClause, mRepSpec);
		}
		catch (PlasticException ex)	{
			String message = "Error getting changesets to be committed: " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}
	}

    @Override
    public String updateWorkspace(File baseDir, String planKey, final String vcsRevisionKey) throws RepositoryException {

        log.debug("updateWorkspace - CI: " +String.format("%s - %s", planKey, vcsRevisionKey));
        final File workspaceFilePath = getWorkingDir(baseDir, planKey);

        try
        {
                return WorkspaceManager.getCacheLock(workspaceFilePath).withLock(new Callable<String>()
                {
                    public String call() throws Exception
                    {
                        WorkspaceManager.createWorkspace(workspaceFilePath);
                        String repSpec = Utils.getRepositorySpec(mRepositoryData);
                        WorkspaceManager.updateWorkspace(workspaceFilePath, repSpec, mRepositoryData.branch, vcsRevisionKey);
                        return vcsRevisionKey;
                    }
                });
        }
        catch (RepositoryException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RepositoryException("An error occurred when updating workspace.", e);
        }
    }
}





