package com.codicesoftware.plugins.bamboo40.changesproviders;

import com.codicesoftware.plugins.bamboo40.PlasticRepositoryData;
import com.codicesoftware.plugins.bamboo40.Constants;


public class ChangesProviderFactory {

    private static ChangesProviderFactory mInstance = null;

    public static ChangesProviderFactory get()
    {
        if (mInstance == null)
            mInstance = new ChangesProviderFactory();
        return mInstance;
    }

    public IChangesProvider getChangesProvider(PlasticRepositoryData repositoryData)  {
        //Currently 2 working modes are available:
        //a)Built-in continuous integration
        //b)PlasticSCM BRANCH_NAME custom branch resolution
        if (repositoryData.branch.indexOf(Constants.BR_NAME) >= 0)
            return new PlasticCustomTestBranchModeProvider(repositoryData);
        return new PlasticContinuousIntegrationMode(repositoryData);
	}
}
