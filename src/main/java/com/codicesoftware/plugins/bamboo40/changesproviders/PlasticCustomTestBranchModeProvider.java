package com.codicesoftware.plugins.bamboo40.changesproviders;

import com.codicesoftware.plugins.bamboo40.PlasticRepositoryData;
import com.codicesoftware.plugins.bamboo40.Constants;
import com.codicesoftware.plugins.bamboo40.Utils;
import com.codicesoftware.plastic.commontypes.BranchInfo;
import com.codicesoftware.plastic.commontypes.ChangesetInfo;
import com.codicesoftware.plastic.query.QueryCommands;
import com.codicesoftware.plastic.query.WhereClauseBuilder;
import com.codicesoftware.plastic.core.PlasticException;
import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.repository.RepositoryException;

import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.Callable;
import java.io.File;

public class PlasticCustomTestBranchModeProvider extends PlasticBaseProvider  {

    //save selected branch on custom mode between getChangesSinceLastBuild and updateWorkspace
    private static Hashtable<String, BranchInfo> mLastResolvedBranchSelectedMap = new Hashtable<String, BranchInfo>();

    public PlasticCustomTestBranchModeProvider(PlasticRepositoryData repositoryData)  {
        super(repositoryData);
    }

    @Override
    public String getChangesSinceLastBuilt(String lastVcsKeyBuilt, List<Commit> newerCommits)
     throws RepositoryException	{
       if (newerCommits == null)
            newerCommits = new ArrayList<Commit>();

        return addCommitsInTestBranchMode(lastVcsKeyBuilt, newerCommits);
    }

    private String addCommitsInTestBranchMode(String lastVcsKeyBuilt, List<Commit> commits)
            throws RepositoryException {

		BranchInfo firstCandidateBranch = pickFirstResolvedBranch();
        String lastCsetOnRep = firstCandidateBranch.getChangeset();
        mLastResolvedBranchSelectedMap.put(lastCsetOnRep, firstCandidateBranch);
        if (lastVcsKeyBuilt.equals(lastCsetOnRep)) {
			return lastVcsKeyBuilt;
		}

		ChangesetInfo[] changesets = getChangesetsToBeCommited(firstCandidateBranch);
		addChangesetsToPendingCommmits(changesets, commits);
        return lastCsetOnRep;
	}

    private BranchInfo pickFirstResolvedBranch() throws RepositoryException {

        BranchInfo[] branches = getResolvedBranches();
		if (branches == null || branches.length == 0)
			return Utils.getBranchInfo(Constants.MAIN, mRepositoryData.repository, mRepositoryData.repositoryserver);
		return branches[0];
	}

    private BranchInfo[] getResolvedBranches() 	throws RepositoryException	{
		try	{
			return QueryCommands.GetResolvedBranches(mRepSpec);
		}
		catch (PlasticException ex) {
			String message = "Error getting resolved branches: " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}
	}

    private ChangesetInfo[] getChangesetsToBeCommited(BranchInfo brInfo) throws RepositoryException {

		WhereClauseBuilder revClause = new WhereClauseBuilder();
		revClause.addClause("changesetid", "=", Long.parseLong(brInfo.getChangeset()));

		try	{
			return QueryCommands.GetChangesets(null, revClause, mRepSpec);
		}
		catch (PlasticException ex)	{
			String message = "Error getting last changeset on branch: " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}
	}

    @Override
    public String updateWorkspace(File baseDir, String planKey, final String vcsRevisionKey) throws RepositoryException {

        log.debug("updateWorkspace - Custom: " +String.format("%s - %s", planKey, vcsRevisionKey));      
        final File workspaceFilePath = getWorkingDir(baseDir, planKey);
        try
        {
                return WorkspaceManager.getCacheLock(workspaceFilePath).withLock(new Callable<String>()
                {
                    public String call() throws Exception
                    {
                        WorkspaceManager.createWorkspace(workspaceFilePath);
                        String repSpec = Utils.getRepositorySpec(mRepositoryData);
                        BranchInfo lastSelectedBranch = mLastResolvedBranchSelectedMap.containsKey(vcsRevisionKey) ?
                                mLastResolvedBranchSelectedMap.get(vcsRevisionKey) : pickFirstResolvedBranch();

                        Utils.writeToFile(lastSelectedBranch.getFullBranchNameWithoutBranchPreffix(), "CurrentBranch.txt");
                        WorkspaceManager.updateWorkspace(
                                workspaceFilePath, repSpec, lastSelectedBranch.getFullBranchNameWithoutBranchPreffix(), vcsRevisionKey);
                        return vcsRevisionKey;
                    }
                });
        }
        catch (RepositoryException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RepositoryException("An error occurred when updating workspace.", e);
        }
    }
}


