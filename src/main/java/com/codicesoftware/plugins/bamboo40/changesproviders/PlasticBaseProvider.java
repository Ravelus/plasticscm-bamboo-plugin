package com.codicesoftware.plugins.bamboo40.changesproviders;

import com.codicesoftware.plugins.bamboo40.PlasticRepositoryData;
import com.codicesoftware.plugins.bamboo40.Utils;
import com.codicesoftware.plastic.core.PlasticException;
import com.codicesoftware.plastic.query.QueryCommands;
import com.codicesoftware.plastic.query.WhereClauseBuilder;
import com.codicesoftware.plastic.commontypes.BranchInfo;
import com.codicesoftware.plastic.commontypes.ChangesetInfo;
import com.codicesoftware.plastic.commontypes.InternalRevisionInfo;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.commit.CommitImpl;
import com.atlassian.bamboo.commit.CommitFile;
import com.atlassian.bamboo.commit.CommitFileImpl;
import com.atlassian.bamboo.author.AuthorImpl;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.ArrayList;
import java.io.File;


public abstract class PlasticBaseProvider implements IChangesProvider {

    protected static final Logger log = Logger.getLogger(PlasticBaseProvider.class);
    protected PlasticRepositoryData mRepositoryData;
    protected String mRepSpec;


    public PlasticBaseProvider(PlasticRepositoryData repositoryData) {
        mRepositoryData = repositoryData;
        mRepSpec = Utils.getRepositorySpec(repositoryData);
    }

    public abstract String getChangesSinceLastBuilt(String lastVcsKeyBuilt, List<Commit> newerCommits) throws RepositoryException;
    public abstract String updateWorkspace(File baseDir, String planKey, String vcsRevisionKey) throws RepositoryException;

    protected void addChangesetsToPendingCommmits(ChangesetInfo[] changesets, List<Commit> commits)
			throws RepositoryException {

		for(ChangesetInfo changeset : changesets) {
			CommitImpl commit = new CommitImpl();
			commit.setAuthor(new AuthorImpl(changeset.getOwner()));
			commit.setDate(changeset.getDate());
			commit.setComment("Branch: " + changeset.getChangesetBranch() +
					"; Changeset num: " + changeset.getChangesetId() +
					". Comments: " +  changeset.getComments());
			List<CommitFile> files = getFiles(changeset.getChangesetId());
			commit.setFiles(files);
			commits.add(commit);
		}
	}

    private List<CommitFile> getFiles(@NotNull String csId)
            throws RepositoryException {
		List<CommitFile> files = new ArrayList<CommitFile>();
		WhereClauseBuilder statusClause = new WhereClauseBuilder();
		statusClause.addClause("changeset", "=", Integer.parseInt(csId));
		InternalRevisionInfo[] revisions;

		try {
			revisions = QueryCommands.GetRevisions(null, statusClause.getWhereString(), mRepSpec);
		}
		catch(PlasticException ex) {
			String message = "Error getting changeset revision: " + ex.getMessage();
			log.warn(message, ex);
			throw new RepositoryException(message);
		}

		if (revisions.length < 1) {
			String message = "Error getting revision branch: no branches returned!";
			log.warn(message);
			throw new RepositoryException(message);
		}

		for(InternalRevisionInfo revision : revisions)	{
			CommitFileImpl commitFile = new CommitFileImpl();
			String filename = revision.getItemPath();
			commitFile.setName(filename);
			commitFile.setRevision("cs:" + csId);
			files.add(commitFile);
		}
		return files;
	}

    protected  File getWorkingDir(File baseDir, String planKey) {
        return new File(baseDir, planKey);
    }
}
