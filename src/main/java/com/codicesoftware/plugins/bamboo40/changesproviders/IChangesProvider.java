package com.codicesoftware.plugins.bamboo40.changesproviders;

import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.repository.RepositoryException;

import java.util.List;
import java.io.File;


public interface IChangesProvider
{
    String getChangesSinceLastBuilt(String lastVcsKeyBuilt, List<Commit> newerCommits) throws RepositoryException;
    String updateWorkspace(File baseDir, String planKey, String vcsRevisionKey) throws RepositoryException;
}
