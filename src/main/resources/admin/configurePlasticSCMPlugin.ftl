<html>
<head>
	<title>[@ww.text name='Plastic SCM Plugin Configuration' /]</title>
	<meta name="decorator" content="adminpage">
</head>
<body>
	<h1>[@ww.text name='Plastic SCM Plugin Configuration' /]</h1>
	<div class="paddedClearer" ></div>
	[@ww.form action="configurePlasticSCMPlugin"
		namespace="/admin"
		id="plasticSCMConfigurationForm"
		submitLabelKey='global.buttons.update'
		cancelUri='/admin/administer.action'
		titleKey='Configure Plastic SCM Plugin'
	]	
		[@ww.textfield
			id='plasticSCMConfigurationForm_cmExe'
			name='cmExePath'
			label='cm Path'
			required='true'
			description='The path of the cm executable.'
		/]
	[/@ww.form]
</body>
</html>
