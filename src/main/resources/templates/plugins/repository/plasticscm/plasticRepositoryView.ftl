[@ui.bambooInfoDisplay titleKey='Plastic SCM Configuration']
	[@ww.label
			label='Repository Name'
			value='${plan.buildDefinition.repository.repositoryname}'
			hideOnNull='false' /]
	[@ww.label
			label='Repository Server (host:port)'
			value='${plan.buildDefinition.repository.repositoryserver}'
			hideOnNull='false' /]

	[@ww.label
			label='Branch'
			value='${plan.buildDefinition.repository.branch}'
			hideOnNull='false' /]
[/@ui.bambooInfoDisplay]
