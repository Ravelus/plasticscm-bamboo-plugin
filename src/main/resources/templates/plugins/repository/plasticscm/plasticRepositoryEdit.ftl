[@ww.textfield
	name='custom.repository.plasticscm.repositoryname'
	label='Repository Name'
	required='true'
	description='Repository name to track'
/]
[@ww.textfield
	name='custom.repository.plasticscm.repositoryserver'
	label='Repository Server (host:port)'
	required='false'
	description='Server where the repository is located'
/]
[@ww.textfield
	name='custom.repository.plasticscm.branch'
	label='Branch'
	required='false'
	description='Branch to track'
/]