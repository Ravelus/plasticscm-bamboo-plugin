How to build the Plastic SCM for Bamboo Plugin

* Download this source code.
* Download and install the atlassian SDK
http://confluence.atlassian.com/display/DEVNET/Setting+up+your+Plugin+Development+Environment

Now you can create an Eclipse project or use the bamboo_plugin.ipr project for IntelliJ 12.